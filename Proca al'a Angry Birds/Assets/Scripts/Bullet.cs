﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WaitTime());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator WaitTime() {
        yield return new WaitForSeconds(10);
        ShootingScript.shottedBullets++;
        ShootingScript.shotCounter = 1;
    }
}
