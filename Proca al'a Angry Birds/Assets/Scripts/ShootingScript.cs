﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingScript : MonoBehaviour
{
    public GameObject bulletPrefab;
    public Transform spawnPosition;
    public float shotSpeed = 5f;
    float shotMultiplier = 1f;
    public Camera mainCamera;

    public static int shotCounter;

    private GameObject spawnedPrefab = null;
    List<GameObject> spawnedBullets = new List<GameObject>();
    public static int shottedBullets = 0;
    // Start is called before the first frame update
    void Start()
    {
        shotCounter = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            InstantPrefab();
        }
        if (Input.GetButtonUp("Fire1"))
        {
            Fire();
        }
        if (spawnedBullets != null)
        {
            Camera.main.transform.position = new Vector3(spawnedBullets[shottedBullets].transform.position.x, 1, -10);
        }
    }

    void InstantPrefab()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.Log("nacisnieto fire1");

        shotMultiplier = spawnPosition.transform.position.x - ray.origin.x * 2;
        if (shotMultiplier < 0)
        {
            shotMultiplier *= -1;
        }
        Debug.Log(ray);
        shotCounter = 0;
    }

    void Fire()
    {
        if (shotCounter == 0)
        {
            //spawnedPrefab = Instantiate(bulletPrefab, spawnPosition.transform.position, spawnPosition.transform.rotation);
            spawnedBullets.Add(Instantiate(bulletPrefab, spawnPosition.transform.position, spawnPosition.transform.rotation));
            //Instantiate(bulletPrefab, spawnPosition.transform.position, spawnPosition.transform.rotation).GetComponent<Rigidbody>().AddForce(new Vector3(1, 0, 0) * shotMultiplier * 10f);
            spawnedBullets[shottedBullets].GetComponent<Rigidbody>().AddForce(new Vector3(1, 0, 0) * shotMultiplier * 10f);
            shotCounter = 2;
        }
    }
}
